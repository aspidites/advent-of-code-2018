module Main where

import Debug.Trace
import System.Environment (getArgs)
import Data.IntSet (IntSet)
import qualified Data.IntSet as S

parseFreqChange :: String -> Int -> Int
parseFreqChange ('+':n) = (+) . read $ n
parseFreqChange ('-':n) = subtract . read $ n
parseFreqChange _ = error "Invalid frequency change"

getFreq :: [String] -> [Int]
getFreq = scanl (\acc x -> (parseFreqChange x) acc) 0

solve :: [String] -> Int
solve xs = go (getFreq $ cycle xs) S.empty
  where
    go :: [Int] -> IntSet -> Int
    go [] ys = trace (show ys) error "Can't find the first duplicate of an empty list"
    go (x:xs) ys 
      | x `S.member` ys = x
      | otherwise = go xs (S.insert x ys)

getInput :: IO [String]
getInput = do
  putStrLn "Reading File..."
  (file:_) <- getArgs
  content <- readFile file
  return $ lines content
      
main :: IO ()
main = do
  input <- getInput
  putStrLn "Solving..."
  print $ solve input
