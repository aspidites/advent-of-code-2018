module Part1 where

parseFreqChange :: String -> Int -> Int
parseFreqChange ('+':n) = (+) . read $ n
parseFreqChange ('-':n) = subtract . read $ n
parseFreqChange _ = error "Invalid frequency change"

getFreq :: [String] -> Int
getFreq = foldr (id . parseFreqChange) 0

main :: IO ()
main = interact ((<> "\n") . show . getFreq . lines)
