module Main where

import System.Environment (getArgs)
import Data.Map (Map)
import qualified Data.Map as M

tests = ["babab", "abbcde"]

histo :: String -> Map Char Int
histo = foldr (\x acc -> M.insertWith (+) x 1 acc) M.empty

getMatches :: Map Char Int -> (Int, Int)
getMatches = M.foldr tally (0, 0)
  where
    tally x (twos, threes)
      | x == 2 = (1, threes)
      | x == 3 = (twos, 1)
      | otherwise = (twos, threes)

solve = checksum . sumMatches . map (getMatches . histo)

sumMatches :: [(Int, Int)] -> (Int, Int)
sumMatches = foldr (\(two, three) (twos, threes) -> (two + twos, three + threes)) (0, 0)

checksum :: (Int, Int) -> Int
checksum (twos, threes) = twos * threes

getInput :: IO [String]
getInput = do
  putStrLn "Reading File..."
  (file:_) <- getArgs
  content <- readFile file
  return $ lines content
      

main :: IO ()
main = do
  input <- getInput
  putStrLn "Solving..."
  print $ solve input
