module Main where

import System.Environment (getArgs)
import Data.Map (Map)
import qualified Data.Map as M

solve :: [String] -> String
solve = uncurry commonChars . checkDifferent . getBoxes . map (getMatches . histo)

histo :: String -> (String, Map Char Int)
histo str = (str, foldr (\x acc -> M.insertWith (+) x 1 acc) M.empty str)

getMatches :: (String, Map Char Int) -> (String, (Int, Int))
getMatches (s, h) = (s, M.foldr tally (0, 0) h)
  where
    tally x (twos, threes)
      | x == 2 = (1, threes)
      | x == 3 = (twos, 1)
      | otherwise = (twos, threes)

getBoxes :: [(String, (Int, Int))] -> [String]
getBoxes = foldr (\(s, (twos, threes)) acc -> if twos > 0 || threes > 0 then s : acc else acc) []

checkDifferent :: [String] -> (String, String)
checkDifferent xs = head [(x, y) | x <- xs, y <- xs, differentChars x y == 1]
  where
    differentChars = compareCharsWith (/=) (\_ acc -> acc + 1) 0

commonChars :: String -> String -> String
commonChars = compareCharsWith (==) (:) ""

compareCharsWith :: (Char -> Char -> Bool) -> (Char -> a -> a) -> a -> String -> String -> a
compareCharsWith comp f def xs ys = foldr (\(l, r) acc -> if (comp l r) then (f l acc) else acc) def chars
  where
    chars = zipWith (,) xs ys

getInput :: IO [String]
getInput = do
  putStrLn "Reading File..."
  (file:_) <- getArgs
  content <- readFile file
  return $ lines content
      

main :: IO ()
main = do
  putStrLn "Solving..."
  --print tests
  --print $ solve tests
  input <- getInput
  putStrLn $ solve input
